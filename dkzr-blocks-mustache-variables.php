<?php
/**
 * Plugin Name: Blocks {{mustache}} variables
 * Plugin URI: https://dkzr.nl/
 * Description: Allow for some {{mustache}} variables to be used in gutenberg blocks.
 * Author: joostdekeijzer
 * Author URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/blocks-mustache-variables
 * Version: 1
 * Text Domain: dkzr-blocks-mustache-variables
 * Domain Path: /languages
 *
 * @see https://mustache.github.io
 */

add_filter( 'render_block', function( $block_content, $parsed_block ) {
  $the_permalink = esc_url( apply_filters( 'the_permalink', get_permalink( get_the_ID() ), get_the_ID() ) );
  return str_replace( '{{the_permalink}}', $the_permalink, $block_content );
}, 10, 2 );
